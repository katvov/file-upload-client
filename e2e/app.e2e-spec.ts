import { FileUploadClientPage } from './app.po';

describe('file-upload-client App', function() {
  let page: FileUploadClientPage;

  beforeEach(() => {
    page = new FileUploadClientPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
