import {Component} from '@angular/core';
import {Message} from 'primeng/components/common/api';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    msgs: Message[];
    note = '';
    email = '';
    uploadedFiles: any[] = [];

    title = 'Загрузка файла';

    /**
     * Add additional fields in query before uploading
     * @param event
     */
    onBeforeUpload(event) {
        let formData: FormData;
        formData = event.formData;
        formData.append('fileuploadbundle_files[note]', this.note);
        formData.append('fileuploadbundle_files[email]', this.email);
        /*console.info(formData);*/
    }

    /**
     * Error processing
     * @param event
     */
    onError(event) {
        this.msgs = [];

        this.msgs.push({severity: 'error', summary: 'Ошибка при передаче файла', detail: ''});
    }

    /**
     * Answer processing
     * @param event
     */
    onUpload(event) {
        for (const file of event.files) {
            this.uploadedFiles.push(file);
        }

        this.msgs = [];

        const ans = JSON.parse(event.xhr.response);
        if (ans === '') {
            this.msgs.push({severity: 'info', summary: 'Спасибо, в скором времени мы вышлем файл-ссылку на ваш личный email', detail: ''});
        } else {
            this.msgs.push({severity: 'error', detail: ans});
        }

        // this.msgs.push({severity: 'info', detail: ans});

    }
}
